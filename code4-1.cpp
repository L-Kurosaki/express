#include <iostream>
#include <string>

using namespace std;

class Personal_info
{
    protected:
    string name;
    string surname;
    string email_address;
    int address;
    int phone_number;


    public:
        Personal_info (string a,string b,string c, int d, int e)
        {
            name = a;
            surname = b;
            email_address =c;
            address = d;
            phone_number = e;
        }

};
class Course_info
{
protected:
    string Qualification;
    int Course_code;
    int Year;
public:
    Course_info ( string j,int k,int l)
    {
        Qualification = j;
        Course_code =k;
        Year = l;
    }
};

class Student: public Personal_info, public Course_info
{
public:
    Student (string w,string x,string y, int z, int v,string vv,int xx,int bb): Personal_info ( w,x,y,z,v),Course_info(vv,xx,bb)
    {

    }
    void display()
    {
        cout<< "NAME:"<<" "<<name<<endl;
        cout<< "SURNAME:"<<" "<<surname<<endl;
        cout << "EMAIL ADDRESS:"<<" "<<email_address<<endl;
        cout << "HOUSE ADDRESS:" << " "<<address<<endl;
        cout << "PHONE NUMBERS:"<<" "<<phone_number<<endl;
        cout << "QUALIFICATION:"<< " "<<Qualification<<endl;
        cout << "COURSE CODE:"<<" "<<Course_code<<endl;
        cout << "YEAR:" <<" "<<Year<<endl;
    }
};


int main()
{
    Student stu("Neo","Matlaila","barsh@gmail.com",664,0732546732,"BSC",172,2);
    Student stu2("Tshepo","Tlou","TshepoT@gmail.com",542,0775465321,"BEd",111,1);
    Student stu3("Tshenolo","Motaung","TshenoMota@gmail.com",444,0765532443,"BSC",212,3);
    Student stu4("Letlhogonolo","Maeng","Lucky47@gmail.com",666,0756432213,"LLB",111,1);
    Student stu5("Keoratile","Selebogo","KeoratileSelebogo@gmail.com",768,0676454357,"BEd",222,3);

    int number;

    cout << " STUDENT AND COURSE INFORMATION "<<endl;
    cout<< " "<<endl;
    cout << "1 Neo Matlaila" <<endl;
    cout << "2 Tshepo Tlou" <<endl;
    cout << "3 Tshenolo Motaung" <<endl;
    cout << "4 Letlhogonolo Maeng" <<endl;
    cout << "5 Keoratile Selebogo" <<endl;
    cout << "9 to exit" <<endl;
    cout <<" "<<endl;
    cout <<"Enter the number to view student information: ";
    cin >>number;
    cout <<endl;

while (number !=9)
    {
    switch(number)
    {
    case 1:
        stu.display();
        break;
    case 2:
        stu2.display();
        break;
    case 3:
        stu3.display();
        break;
    case 4:
        stu4.display();
        break;
    case 5:
        stu5.display();
        break;
    default:
        cout << "Choose options from above!" <<endl;
    }
    cout << " "<<endl;
    cout << "STUDENT AND COURSE INFORMATION "<<endl;
    cout<< " "<<endl;
    cout << "1 Neo Matlaila" <<endl;
    cout << "2 Tshepo Tlou" <<endl;
    cout << "3 Tshenolo Motaung" <<endl;
    cout << "4 Letlhogonolo Maeng" <<endl;
    cout << "5 Keoratile Selebogo" <<endl;
    cout << "9 to exit" <<endl;
    cout <<" "<<endl;
    cout <<"Enter the number to view student information: ";
    cin >>number;
    cout <<endl;
    }

    return 0;
}
